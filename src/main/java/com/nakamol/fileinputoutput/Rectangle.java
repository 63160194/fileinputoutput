/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.fileinputoutput;

import java.io.Serializable;

/**
 *
 * @author OS
 */
public class Rectangle implements Serializable{
    private int wide;
    private int height;
    
    public Rectangle(int wide, int height){
        this.wide = wide;
        this.height = height;
    }

    public int getWide() {
        return wide;
    }

    public void setWide(int wide) {
        this.wide = wide;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "wide=" + wide + ", height=" + height + '}';
    }
    
}
